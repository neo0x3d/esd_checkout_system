#include <unistd.h>
#include <iostream>
#include "customer_display.hpp"
#include <bits/stdc++.h> 
#include <stdlib.h>

using namespace std;

int main(){
    
    CustomerDisplayDriver cd;
    char c;

    cd.initDisplay();
   
    //CONFIRMAR SE \N COLOCA O CURSOR NA LINHA DE BAIXO

    //demo to test the Customer Display
    //clear display
  
    //Overwrite Demo

    cd.printToLine("OVER WRITE DEMO ", LINE_1);
    sleep(3);
    cd.clear();
    cd.printToLine("OVER_WRITE_MODE  1", LINE_1);
    sleep(3);
    cd.printToLine("OVER_WRITE_MODE  2", LINE_2);

    cd.clearLine(LINE_1);
    sleep(3);
    cd.clearLine(LINE_2);

    
    //VERTICAL SCROLL
    cd.clear();
    sleep(3);
    cd.printToLine("Vertial Demo  ", LINE_1);
    sleep(3);
    cd.clear();
    cd.printLineInVerticalMode("Vertical Line 1 ");

    sleep(3);
    cd.printLineInVerticalMode("Vertical Line 2 ");
    sleep(3);
    cd.printLineInVerticalMode("Vertical Line 3 ");
    sleep(3);
    sleep(3);
    //HORIZONTAL SCROLL
    cd.clear();

    cd.printToLine("Horizontal Demo  ", LINE_1);
    sleep(3);
    cd.clear();
    cd.printUpperLineInHorizontalMode(" Horizontal Line 1");
    sleep(3);
    cd.printUpperLineInHorizontalMode(" Horizontal Line 2");
    sleep(3);
    cd.printUpperLineInHorizontalMode(" Horizontal Line 3");
    sleep(3);

    //PRINT in SLIDING MODE
    sleep(3);
    cd.printToLine("Sliding Demo  ", LINE_1);
    sleep(3);
    cd.clear();
    cd.printUpperLineInSlidingMode("    Printing in Sliding Mode only on upper line, Good Bye");
   

   

    
    
   

    
}