
#include <string>
#include <vector>
#include "EPSON.hpp"
#include "USB.hpp"

#define MAX_lineS 2
#define LINE_1 "\x1"
#define LINE_2 "\x2"
#define MAX_COLUMNS 20
#define DEVICE "/ttyUSB0" // pc

//#define DEVICE "/bus/usb/001/011" //zybo

class CustomerDisplayDriver{
    private:

        std::string data;
        USB usb;
        EPSON epson_command;

      
        void sendCommand(std::vector<std::string>  command);
        void printLine(std::vector<std::string> &command, std::string text);
        void setUsb(std::string device_name);
        void setEpsonProtocol();

    public:
        CustomerDisplayDriver();
        void initDisplay(void);

        void printToLine(std::string text, std::string  line_number);//line_number only relevant for Overwrite mode
        void printUpperLineInSlidingMode(std::string text);
        void printLineInVerticalMode(std::string text);
        void printUpperLineInHorizontalMode(std::string text);

        void clear();
        void clearLine(std::string line_number);
        void turnOffDisplay();

};