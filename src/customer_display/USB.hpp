#include <string>
#include <fstream>

class USB{
    
    private:
     
        std::string DevicePath;
        std::ofstream device;
      
     public:
        void writeToUsb(std::string data);
        void closeConnection(void);
        void setDevice(std::string path);


};