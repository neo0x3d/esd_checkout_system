//EPSON ESC/POS command list
#include <vector>
#include <string>


#define CLEAR_DISPLAY 0
#define INITIALIZE_DISPLAY 1
#define OVER_WRITE_MODE 2
#define SELECT_DEVICE 3
#define MOVE_CURSOR_RIGHT 4
#define MOVE_CURSOR_LEFT 5
#define MOVE_CURSOR_UP 6 
#define MOVE_CURSOR_DOWN 7
#define MOVE_CURSOR_RIGHT_END 8
#define MOVE_CURSOR_LEFT_END 9
#define MOVE_CURSOR_HOME 10
#define MOVE_CURSOR_BOTTOM 11
#define CLEAR_CURSOR_LINE 12
#define MOVE_CURSOR_TO_POSITION 13

#define BAUD_RATE 14
#define USA_CHAR 15
#define PROTOCOL 16
#define DEMO 17
#define COMMUNICATION 18

#define WRITE_TO_UPPER_LINE 19
#define WRITE_TO_LOWER_LINE 20

#define VERTICAL_SCROLL_MODE 22
#define HORIZONTAL_MODE 23


#define CURSOR_ON 24
#define CURSOR_OFF 25
#define SCROLL_MSG_CONTINUOUSLY 26

class EPSON{
    private:
        
        std::vector<std::string> baud_rate{"\x02","\x05","\x42","\x30","\x03"}; //9600
        std::vector<std::string> usa_char{"\x02","\x05","\x53","\x30","\x03"};//usa
        std::vector<std::string> epson_protocol{"\x02","\x05","\x53","\x43","\x31","\x03"};
        std::vector<std::string> demo{"\x02","\x05","\x44","\x08","\x03"};
        std::vector<std::string> communication{"\x02","\x05","\x50","\x31","\x03"}; //N-8-1

        std::vector<std::string> clear_display{"\x0C"};
        std::vector<std::string> clear_cursor_line{"\x18"};

        std::vector<std::string> initialize_display{"\x1B", "\x40"};
        std::vector<std::string> over_write_mode{"\x1F","\x01"};
        std::vector<std::string> vertical_scroll_mode{"\x1F","\x02"}; 
        std::vector<std::string> horizontal_scroll_mode{"\x1F","\x03"};

        std::vector<std::string> select_device{"\x1B","\x3D","\x02"};


        std::vector<std::string> move_cursor_right{"\x09"};
        std::vector<std::string> move_cursor_left{"\x08"};
        std::vector<std::string> move_cursor_up{"\x1F","\x0A"};
        std::vector<std::string> move_cursor_down{"\x0A"};
        std::vector<std::string> move_cursor_right_end{"\x1F", "\x0D"};
        std::vector<std::string> move_cursor_left_end{"\x0D"};
        std::vector<std::string> move_cursor_home{"\x0B"};
        std::vector<std::string> move_cursor_bottom{"\x1F","\x42"};
        std::vector<std::string> move_cursor_to_specified_position{"\x1F", "\x24", "", ""};// x and y <=20 & >=1
   
        std::vector<std::string> write_to_upper_line{"\x1B","\x51","\x41","","\x0D"};
        std::vector<std::string> write_to_lower_line{"\x1B","\x51","\x42","","\x0D"};
        std::vector<std::string> scroll_message_continuously{"\x1B","\x51","\x44","","\x0D"};
        std::vector<std::string> turn_on_cursor{"\x1B", "\x5F", "\x1"};
        std::vector<std::string> turn_off_cursor{"\x1B", "\x5F", "\x0"};

    public:

        std::vector<std::string> getCommand(int  command);
        std::vector<std::string> moveCursorToPosition(std::string row, std::string line);


};