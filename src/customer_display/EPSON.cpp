#include "EPSON.hpp"
#include <string>
#include <vector>



std::vector<std::string> EPSON::getCommand(int command){

    switch (command)
    {
        case CLEAR_DISPLAY:
        {   
            return this->clear_display;
        }
            

        case INITIALIZE_DISPLAY:
        {
            return this->initialize_display;
        }
            
        case OVER_WRITE_MODE:
        {
            return this->over_write_mode;
        }
            
        case SELECT_DEVICE:
        {
            return this->select_device;
        }
            

        case MOVE_CURSOR_BOTTOM:
        {
            return this->move_cursor_bottom;
        }
            

        case MOVE_CURSOR_DOWN:
        {
            return this->move_cursor_down;
        }
            

        case MOVE_CURSOR_HOME:
        {
            return this->move_cursor_home;
        }
            

        case MOVE_CURSOR_LEFT:
        {
            return this->move_cursor_left;
        }
            

        case MOVE_CURSOR_LEFT_END:
        {
            return this->move_cursor_left_end;
        }
            

        case MOVE_CURSOR_RIGHT:
        {
            return this->move_cursor_right;
        }
            

        case MOVE_CURSOR_RIGHT_END:
        {
            return this->move_cursor_right_end;
        }
            

        case MOVE_CURSOR_UP:
        {
            return this->move_cursor_up;
        }
            
        case BAUD_RATE:
        {
            return this->baud_rate;
        }
        case USA_CHAR:
        { 
            return this->epson_protocol;
        }
        case DEMO:
        {
            return this->demo;
        }
        case COMMUNICATION:
        {
            return this->communication;
        }
            
        case PROTOCOL:
        { 
            return this->epson_protocol;
        }
        case CLEAR_CURSOR_LINE:
        {
            return this->clear_cursor_line;
        }
        case WRITE_TO_LOWER_LINE:{
            return this->write_to_lower_line;
        }
        case WRITE_TO_UPPER_LINE:{
            return this->write_to_upper_line;
        }
        case CURSOR_ON:{
            return this->turn_on_cursor;
        }
        case CURSOR_OFF:{
            return this->turn_off_cursor;
        }
        case VERTICAL_SCROLL_MODE:{
            return this->vertical_scroll_mode;
        }
        case HORIZONTAL_MODE:{
            return this->horizontal_scroll_mode;
        }   
        case SCROLL_MSG_CONTINUOUSLY:{
            return this->scroll_message_continuously;
        }
        case MOVE_CURSOR_TO_POSITION:{
            return this->move_cursor_to_specified_position;
        }
        default:
            return this->clear_display;
    }

}
std::vector<std::string> EPSON::moveCursorToPosition(std::string row, std::string line){

    std::vector<std::string> command;
    
    command=getCommand(MOVE_CURSOR_TO_POSITION);
    int flag=0;

    for(int i=0; i<command.size(); i++){

        if(command[i]=="" && flag==0){
            command[i]=row;
            flag=1;
        }
        else if(command[i]=="" && flag!=0)
        {
            command[i]=line;
        }
        
    }

    return command;

}