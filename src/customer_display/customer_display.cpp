#include "customer_display.hpp"
#include <vector>
#include <iostream>
#include <unistd.h>
#include <string>

//private
void CustomerDisplayDriver::setEpsonProtocol(){
    sendCommand(this->epson_command.getCommand(BAUD_RATE));
    sendCommand(this->epson_command.getCommand(USA_CHAR));
    sendCommand(this->epson_command.getCommand(PROTOCOL));
    sendCommand(this->epson_command.getCommand(COMMUNICATION));
   
}

void CustomerDisplayDriver::sendCommand(std::vector<std::string>  command){
    
    std::string text;
    this->usb.closeConnection();
    this->usb.setDevice(DEVICE);
    for(int i=0; i<command.size();i++){
        text+=command[i];
          
    }

    this->usb.writeToUsb(text);

}

void CustomerDisplayDriver:: printLine(std::vector<std::string> &command, std::string text){
    
    std::vector<std::string>   data, aux;
    int flag=0;
    aux=epson_command.getCommand(SCROLL_MSG_CONTINUOUSLY);

    if(aux==command)
        flag=1;

    for(int i=0; i<command.size();i++){

        if(!command[i].compare("") ) // fill in the command the string text
        {   
            //check for +20 characters
            if(!flag){

                if(text.size()<=MAX_COLUMNS){
                    data.push_back(text);
                }

                else
                {   //print only the first 20 chars

                    std::string aux;

                    for(int i=0; i<MAX_COLUMNS;i++){
                        
                        aux.push_back(text[i]);
                        
                    }
                    data.push_back(aux);
                }
            }
            else
            {
                data.push_back(text);
            }
        }
        else
        {
            data.push_back(command[i]);
        }

    }
    sendCommand(data);
}


void CustomerDisplayDriver::setUsb(std::string device_name){
    this->usb.setDevice(DEVICE);
}

void CustomerDisplayDriver::printUpperLineInHorizontalMode(std::string text ){
    std::vector<std::string> command;
    //set mode
    sendCommand(epson_command.getCommand(HORIZONTAL_MODE));
    sendCommand(epson_command.getCommand(MOVE_CURSOR_HOME));
    this->usb.closeConnection();
    this->usb.setDevice(DEVICE);
    this->usb.writeToUsb(text);

}
void CustomerDisplayDriver::printLineInVerticalMode(std::string text){
    std::vector<std::string> command;

    sendCommand(epson_command.getCommand(VERTICAL_SCROLL_MODE));
    this->usb.closeConnection();
    this->usb.setDevice(DEVICE);
    this->usb.writeToUsb(text);

}
void CustomerDisplayDriver::printUpperLineInSlidingMode(std::string text){

    clear();
    std::vector<std::string>  command;
    command=epson_command.getCommand(SCROLL_MSG_CONTINUOUSLY);
    printLine(command, text);
  
}
//public

CustomerDisplayDriver::CustomerDisplayDriver(){

    setUsb(DEVICE);// configure usb port baudrate  9600, 8 bits, non parity, 1 stop bit
    setEpsonProtocol(); 
}

void CustomerDisplayDriver::initDisplay(void){

    sendCommand(epson_command.getCommand(INITIALIZE_DISPLAY));
    sendCommand(epson_command.getCommand(OVER_WRITE_MODE));


}

void CustomerDisplayDriver::printToLine(std::string text, std::string  row_number){
    
    std::vector<std::string>  command;
    sendCommand(epson_command.getCommand(OVER_WRITE_MODE));

    try{
        int error;

        if(LINE_1 ==row_number){
            command=epson_command.getCommand(WRITE_TO_UPPER_LINE);
            printLine(command, text);
            if(text.size()>20)
                error=2;

        }
        else if(LINE_2 ==row_number){
            command=epson_command.getCommand(WRITE_TO_LOWER_LINE);
            printLine(command, text);
            if(text.size()>20)
                error=2;
        }

        else{
            
            throw error=1;
        }
    }

    catch(int error){
        if(1==error)
            std::cout<<"Message not pinted\n"<<"Usage: row number = 1 for 1st line, rowm number = 2 for 2nd line\n";
        else if (error==2)
            std::cout<<"only the first"<<MAX_COLUMNS <<" chars are printed in each line of the display\n";
    }

}

void CustomerDisplayDriver::clear(){
    sendCommand(this->epson_command.getCommand(CLEAR_DISPLAY) );
}

void CustomerDisplayDriver::clearLine(std::string row_number){

    try{
        std::vector<std::string>  command;

        if(LINE_1 ==row_number){
            //send empty message = clear the line
            printToLine("",LINE_1);  
            
        }
        else if(LINE_2 ==row_number){
            //send empty message = clear the line
            printToLine("",LINE_2);   
    
        }

        else{
            int error;
            throw error=1;
        }
    }
    catch(int error){
    
        std::cout<<"Message not printed\n"<<"Usage: row number = 1 for 1st line, rowm number = 2 for 2nd line\n";
    }
    
}
void  CustomerDisplayDriver::turnOffDisplay(){

    clear();
    this->usb.closeConnection();
}