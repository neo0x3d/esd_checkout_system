#include "USB.hpp"

#include <iostream>
//sudo chmod 666 /dev/ttyUSB0 
//lsusb
//SCRIPT BEFORE RUNNING COMMAND
#include <fstream>
#include "EPSON.hpp"

#define DEV_PATH  "/dev"

//public

void USB::writeToUsb(std::string data){
    int error;
    
    try{
            device<<data;

            if (device.bad()){
                error=1;
            }

    }

    catch(int error){
         std::cout<<"Could not write to device "<<this->DevicePath<<std::endl;
    }

}
void USB::closeConnection(void){
    device.close();
}

void USB::setDevice(std::string usb_port){
    std::string path;
    std::string error;

    path=DEV_PATH+usb_port;

    try{
        device.open(path,  std::ios::app);
        if(!device.is_open()){
            error="Could not open " + path;
        }
        
    } 
    catch (std::string error){
        std::cout<<error<<std::endl;
    }

}