# Notes from the lecture on 3.12.2019 about database/socket interface, feel free to modify

# Should provide following methods:

* get item by id/name/barcode
* create new receipt
* get receipt by number
* complete transaction

# zmq (zeromq)
Before sending a request (from mq to client or client to mq) the request/reply is sent to the appropriate receiver. The request/reply size can be used to allocate a buffer on the reciever side and will be answered with a ack. After the ack got received the actual request can be sent. 

Explanation in diagram style: 

+------+                 +------+
|Client|                 |Server|
+---+--+                 +------+
    |                        |
    |   send request size    |
    +------------------------>
    |                        |
    |          ack           |
    <------------------------+
    |                        |
    |       request          |
    +------------------------>
    |                        |
    |    send reply size     |
    <------------------------+
    |                        |
    |       ack              |
    +------------------------>
    |                        |
    |      send reply        |
    <------------------------+
    |                        |
    |                        |

