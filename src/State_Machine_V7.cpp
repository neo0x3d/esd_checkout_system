/*
Checkout program. 
State Machine.
*/

/*
Todo:
Fix bug with line edit in edit receipt option (not updating line numbers)
Add in a security check of the cash amount, what if it's less than payment due?
Look at all the extra options needed
Fix that one bug, or not, who really cares

Done:
Differenciate between option or barcode
Add arrays to store data (have to use vectors)
For the price, an int is not the best type to store it as - use float instead
Publish to git - done
Get real total by multiplying quantity and price - total_item_price_arr
Calculate the grand total - grand_total
Send the total to the payment function - this is kinda done no?
Can also create a mini database if things don't work out, also need to add item name - 
Add some more options to the payment system - 
Clear the arrays when payment is made - clear_contents
Add line count - line_arr
Fix bug with total price - fixed but not in a "nice" way but ah sure look it, yolo
Allow user to edit receipts - check evernote for steps
Maybe look into switch cases as another way of doing the state machine - too late for that

*/


#include <iostream>
#include <string>
#include <vector>       //For arrays
#include <algorithm>    //For vector search

#include <chrono>       //To sleep after payment
#include <thread>       //To sleep after payment

using namespace std;

string input;
string str;
int item_quantity;
string multiple_item_quantity; 
float price;                        // *
float total_price;                  // *
float grand_total;

string input_payment;               //For payment system
float change_due;                   //Change due back to the customer
float input_payment_float;
string temp;                        //Just as a temp holder

int line_number = 0;
int index_number_from_line;
int edit_line_number; 

vector<string> barcode_arr;         //For storing barcodes
vector<int> quantity_arr;           //For storing quantities
vector<float> price_arr;            //For storing prices *
vector<float> total_item_price_arr; //For storing the price multiplied by quantity *
vector<string> name_arr;            //For name of item
vector<int> line_arr;               //For line number

//------------------------------------------------------------
//The stuff between these lines is purely for the fake database
vector<string> mini_database_barcode_arr;
vector<string> mini_database_name_arr;
vector<float> mini_database_price_arr;
//This bit here could be done much nicer, but I like this way so fight me
//vector<int> mini_database_arr = { 12, 45, 54, 33, 2, 7, 8, 22, 43, 19 };
int pop_database_barcodes(){
    mini_database_barcode_arr.push_back("198225046");
    mini_database_barcode_arr.push_back("350423528");
    mini_database_barcode_arr.push_back("353214953");
    mini_database_barcode_arr.push_back("495957820");
    mini_database_barcode_arr.push_back("541831240");
    mini_database_barcode_arr.push_back("576966938");
    mini_database_barcode_arr.push_back("595863245");
    mini_database_barcode_arr.push_back("664287232");
    mini_database_barcode_arr.push_back("692439503");
    mini_database_barcode_arr.push_back("756545720");
}
//List of item names
int pop_database_names(){
    mini_database_name_arr.push_back("Wii       ");
    mini_database_name_arr.push_back("Kerrygold ");
    mini_database_name_arr.push_back("Taytos    ");
    mini_database_name_arr.push_back("Cashel    ");
    mini_database_name_arr.push_back("WD40      ");
    mini_database_name_arr.push_back("MiWadi    ");
    mini_database_name_arr.push_back("Wonderwall");
    mini_database_name_arr.push_back("Bog Oak   ");
    mini_database_name_arr.push_back("Aran      ");
    mini_database_name_arr.push_back("Cadbury   ");
}
//List of item prices
int pop_database_prices(){
    mini_database_price_arr.push_back(256);
    mini_database_price_arr.push_back(1.65);
    mini_database_price_arr.push_back(1);
    mini_database_price_arr.push_back(2.64);
    mini_database_price_arr.push_back(6.05);
    mini_database_price_arr.push_back(4.99);
    mini_database_price_arr.push_back(11);
    mini_database_price_arr.push_back(148);
    mini_database_price_arr.push_back(199);
    mini_database_price_arr.push_back(2);
}

//Search the fake database
int search_databse_and_fill(string input){
    vector<string>::iterator it = find(mini_database_barcode_arr.begin(), mini_database_barcode_arr.end(), input);
    if (it != mini_database_barcode_arr.end()){
        //element found
        // Get index of element from iterator
        int index = distance(mini_database_barcode_arr.begin(), it);
        //cout << "\nIndex of item: " << index << endl;
        name_arr.push_back(mini_database_name_arr[index]);
        price_arr.push_back(mini_database_price_arr[index]);

        //Calculate the total for the number of items
        //I know it's a shitty way to do it but at least it works ...
        index_number_from_line = line_number -1 ;
        total_price = quantity_arr[index_number_from_line] * mini_database_price_arr[index];
        total_item_price_arr.push_back(total_price);


    }else{
	    cout << "\nElement Not Found" << endl;
    }
}


//------------------------------------------------------------
//Functions used by the main loop and payment process

// Returns true if s is a number else false 
bool isNumber(string s) 
{ 
	for (int i = 0; i < s.length(); i++) 
		if (isdigit(s[i]) == false) 
			return false; 

	return true; 
} 

//Calculates the grand total the customer has to pay
int calculate_grand_total(){
    grand_total = 0;
    for (int i = 0; i < barcode_arr.size(); i++){
        grand_total = grand_total + total_item_price_arr[i];
    }
}

//Print all the items scanned and their quanity
int print_items(){
    cout << "The items scanned:\n"; 
    for(int i = 0; i < barcode_arr.size(); i++) {
    cout<< "Line: " << line_arr[i]
        << "\tBarcode: " << barcode_arr[i]
        << "\tName: " << name_arr[i] 
        << "\tQuantity: " << quantity_arr[i] 
        << "\tItem Cost: " << price_arr[i]
        << "\tTotal Price: " << total_item_price_arr[i] << endl;}
    
    //cout<< "\n\nThe grand total is: " << grand_total <<endl;
    return 0;
}

//Clear the history of scanned items
int clear_contents(){
    cout << "Clearing memory" << endl;
    barcode_arr.clear();
    name_arr.clear();
    quantity_arr.clear();
    price_arr.clear();
    total_item_price_arr.clear();
    grand_total = 0;
    line_number = 0;
}










//This would be to enter the payment system
int PaymentProcess (){
    cout<<"####################"<<endl;
    cout<<"#  Payment System  #"<<endl;
    cout<<"####################"<<endl;

    calculate_grand_total();    // Calculate the grand total
    print_items();              //print in terminal all the items
    
    cout << "\nThe total to pay is: " << grand_total << " DKK" << endl;
    cout << "\nCash or credit?[cash amout, C]: ";

    cin >> input_payment;       //Wait for either a cash amount OR credit card option

        if (isNumber(input_payment)) {
            //A number was entered, so the cash amount
            cout << "\nCustomer is paying by cash" << endl;
            input_payment_float = atof(input_payment.c_str());  //Convert input to string
            change_due = grand_total - input_payment_float;     //Calculate change due
            cout << "Customer handed cashier: " << input_payment_float << " DKK" << endl;
            cout << "Their change due is: " << change_due << endl;

            cout << "Waiting 10 seconds for you to hand them their change" << endl;
            this_thread::sleep_for(std::chrono::milliseconds(10000));

            //At this point the transaction is complet, should display a message and restart
            clear_contents();   //Clear the scanned items


        }else{
            //Not a number so they want to pay by card
            if (input == "C") {
                cout << "\n Customer is paying by card\n\n";
                //Here we would go to the card payment process

            } else {
                cout << "\nInput not regonised in payment system" << endl;
            }
        }

    return 0;

}



int edit_items_list(){
    //Print this stuff so you know what you can edit
    calculate_grand_total();    
    print_items();

    //First select a line
    cout << "\nChose line to edit: "; 
    cin >> edit_line_number;

    edit_line_number = edit_line_number - 1;    //Correct since line starts at 1, array at 0

    //Print that line
    cout<< "Line: " << line_arr[edit_line_number]
        << "\tBarcode: " << barcode_arr[edit_line_number]
        << "\tName: " << name_arr[edit_line_number] 
        << "\tQuantity: " << quantity_arr[edit_line_number] 
        << "\tItem Cost: " << price_arr[edit_line_number]
        << "\tTotal Price: " << total_item_price_arr[edit_line_number] << endl;

    //1.Employee presses ‘edit receipt’ and selects the line to be edited.
    //2. Displays the line on the employee display
    //3. Employee selects quantity, price or delete line.
    //4.Update receipt 

    cout << "What would you like to edit?" << endl;
    cout << "Delete line [D]" << endl;
    cout << "Change quantity [Q]" << endl;
    cout << "Change price [P]" << endl;
    cout << "Option: ";
    cin >> input;

    if (input == "D"){
        //Delete line
        line_arr.erase(line_arr.begin()+edit_line_number);
        barcode_arr.erase(barcode_arr.begin()+edit_line_number);
        name_arr.erase(name_arr.begin()+edit_line_number);
        quantity_arr.erase(quantity_arr.begin()+edit_line_number);
        price_arr.erase(price_arr.begin()+edit_line_number);
        total_item_price_arr.erase(total_item_price_arr.begin()+edit_line_number);

    }else if (input == "Q"){
        //Change quantity
        cout << "Enter the new quantity: ";
        cin >> item_quantity;
        quantity_arr.at(edit_line_number) = item_quantity;
        
        total_price = item_quantity * price_arr[edit_line_number];
        total_item_price_arr.at(edit_line_number) = total_price;
        

    }else if (input == "P"){
        //Change price
        cout << "Enter the new price: ";
        cin >> price;
        price_arr.at(edit_line_number) = price;                 // Update individual price
        
        total_price = quantity_arr[edit_line_number] * price;
        total_item_price_arr.at(edit_line_number) = total_price;//Update the total price

    }



}




//----------------------------------------------------------------------------------
//Use this to declare and setup everything

int main(){

    cout << "Starting the checkout program.\n\n\n";

    //Setup stuff for the fake database setup
    pop_database_barcodes();    //Populate the database
    pop_database_names();       //Same but for the names
    pop_database_prices();      //Same bur prices


    cout<<"####################"<<endl;
    cout<<"# Checkout System  #"<<endl;
    cout<<"####################"<<endl;

    cout << "\n\n[B] Calculate total and go to Payment Process.\n"
         << "[P] To print a list of current items.\n"
         << "[E] To edit the list of current items.\n"
         << "[x] Set how many items you want before scanning barcode.\n"
         << "Or you can just scan the barcode of your item\n"
         << "[kill] will end the proram.\n";

    while(true){

        cout << "\n\nPlease scan your item or select an option[B,P,E,x,kill]: ";

        //Input should be either a key input OR the scanned barcode
        //"input" is then checked agains the possible options. 
        cin >> input;

        if (isNumber(input)) {
            // A barcode was scanned - add it to the list of items 
            cout << "A barcode waw scanned, added to list.\n";
            
            //From here, the database should be checked for the barcode 

            barcode_arr.push_back(input);   //Add barcode to array
            quantity_arr.push_back(1);      //Default to one item
            
            line_number = line_number + 1;
            line_arr.push_back(line_number);

            //Search the fake database for info and add to array
            //Used to populate price and name array
            search_databse_and_fill(input);
    
        }else{  // An option was pressed, make a decision based on the input
            
            if (input == "B") {
                // Total - Print total and go to payment system
                cout << "Going to payment\n\n";
                PaymentProcess();

            } else if (input == "x") {
                // Propmpt the user to enter the number of items they want then wait for barcode input
                cout << "Enter the number of items: "; //Don't enter a string here, seriously! 
                cin >> item_quantity;
                quantity_arr.push_back(item_quantity);
                
                cout << "Please scan the barcode of the tiem: ";
                cin >> multiple_item_quantity;
                barcode_arr.push_back(multiple_item_quantity);

                line_number = line_number + 1;
                line_arr.push_back(line_number);
                
                search_databse_and_fill(multiple_item_quantity);
                
            } else if (input == "P"){
                //Print what you have scanned till now
                calculate_grand_total();    // Calculate the grand total
                print_items();              //Print the list
            } else if (input == "E"){
                //Edit receipt
                cout << "Going to edit list of items mode." << endl;
                edit_items_list();

            } else if (input == "kill") {
                //Exit the whole program
                //imgur.com/dMiLh6N.jpg
                break;
            }else{
                //My brain while trying to get this thing done imgur.com/wTKR4kZ.mp4
                cout << "Action not recognised, invalid input!";
            }

        }
    }




    cout << "\n\n\nEnd of Main loop\n";
    cout<<"####################"<<endl;
    cout<<"#     Goodbye      #"<<endl;
    cout<<"####################"<<endl;
    return 0;
}



