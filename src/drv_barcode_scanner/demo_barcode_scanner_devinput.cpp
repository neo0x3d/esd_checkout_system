#include <pthread.h>
#include <string>
#include <iostream>
#include <mqueue.h>
#include <cstring>
#include <unistd.h>
#include <fstream>
#include <linux/input.h>

// mq can remain on the system with old size, remove from /dev/mqeue/ if size changes
#define MY_QUEUE "/my_queue"
#define MAX_SIZE 30
#define MAX_COUNT 13

//#define KEYDEV "/dev/input/by-path/platform-i8042-serio-0-event-kbd"
//#define KEYDEV "/dev/input/event3" // define if CIN or the actual device should be used
//#define KEYDEV "/dev/input/by-id/usb-USB_Adapter_USB_Device-event-kbd"
#define KEYDEV "/dev/input/event3"


class BCReader {
public:
    BCReader(const std::string path);
    ~BCReader();
    int getBarcode(std::string* target);
    static int checkBarcode(std::string barcode);
    bool performcheckBarcode = true;
private:
    std::ifstream charDev;
};


BCReader::BCReader(const std::string path)
{
    //std::ifstream kbyd;
    //ioctl(keyboard_fd, EVIOCGRAB, 1);
    charDev.open(path, std::ifstream::in);
    if(charDev.fail())
    {
        std::cout << "error opening chardev for reading key events: " << strerror(errno) << std::endl << "Exiting. " << std::endl;
        exit(-1);
    }
}

BCReader::~BCReader()
{
    charDev.close();
}

/** Check if the barcode could be a valid ean_13 or upc_a barcode.
* @param barcode
* @return 0 if found to be correct, -1 otherwise
*/
int BCReader::checkBarcode(std::string barcode)
{
    if((barcode.length() != 12) && (barcode.length() != 13))
    {
        std::cerr << "lengh not good: " << barcode.length() << std::endl;
        return -1;
    }
    for(int i=0; i<barcode.length(); i++)
    {
        if(!isdigit(barcode[i]))
            return -1;
    }
    return 0;
}

int BCReader::getBarcode(std::string* target)
{
    std::string buffer;
    struct input_event InputEvent;
    char data [sizeof(InputEvent)];
    bool readyToSend;


    while(!readyToSend)
    {
        charDev.read(data, sizeof(data));
        memcpy(&InputEvent, data, sizeof(InputEvent));

        // only reacting on keyboard keys and pressed (otherwise it would fire at all state changes)
        if(InputEvent.type == EV_KEY && InputEvent.value == 1)
        {
            readyToSend = false;
            // key maping for numbers and numpad
            switch(InputEvent.code)
            {
            case KEY_0:
            case KEY_KP0:
                buffer.append("0");
                break;
            case KEY_1:
            case KEY_KP1:
                buffer.append("1");
                break;
            case KEY_2:
            case KEY_KP2:
                buffer.append("2");
                break;
            case KEY_3:
            case KEY_KP3:
                buffer.append("3");
                break;
            case KEY_4:
            case KEY_KP4:
                buffer.append("4");
                break;
            case KEY_5:
            case KEY_KP5:
                buffer.append("5");
                break;
            case KEY_6:
            case KEY_KP6:
                buffer.append("6");
                break;
            case KEY_7:
            case KEY_KP7:
                buffer.append("7");
                break;
            case KEY_8:
            case KEY_KP8:
                buffer.append("8");
                break;
            case KEY_9:
            case KEY_KP9:
                buffer.append("9");
                break;
            case KEY_ENTER:
            case KEY_KPENTER:
                std::cout << "rec enter" << std::endl;
                readyToSend = true;
                break;
            default:
                std::cout << "Unrecognized key: " << InputEvent.code << std::endl;
                break;
            }
        }
    }
    *target =  buffer;
    return (*target).length();
}


/** Get barcode from cin or /dev/input/
*
*/
void* get_barcode(void *ptr) {

    std::string buffer;

    bool toSend = false;

    mqd_t message_queue = mq_open(MY_QUEUE, O_CREAT | O_WRONLY);

    if (message_queue == (mqd_t) - 1)
    {
        std::cout << "Producer, Error: " << strerror(errno) << std::endl << "Exiting. " << std::endl;
        exit(-1);
    }
    
    // create new reader object
    BCReader barcode(KEYDEV);
    barcode.performcheckBarcode = false;
    
    while(1) {// check if read failed, then abort
        barcode.getBarcode(&buffer);
        //std::cout << "printing cont: "<< buffer << std::endl;
        //std::cin >> buffer;

        //check if a valid barcode was read
        if(barcode.performcheckBarcode == true)
        {
            if(BCReader::checkBarcode(buffer) == -1)
            {
                std::cerr << "barcode's fuckered: " << buffer << std::endl;
                continue;
            }
        }

        if(buffer.size() > 1)
        {

            // sanitize input here

            int ret = mq_send(message_queue, buffer.c_str(), strlen(buffer.c_str())+1, 0);
            //std::cout << "outstring:" << buffer.c_str() << "-"<< std::endl;
            //std::cout << "len: " << strlen(buffer.c_str())+1 << std::endl;
            //toSend = false;
            buffer.clear();
            if (ret < 0) {
                std::cout << "Error sending: " << strerror(errno) << std::endl;
                exit(-1);
            }
        }
    }

    mq_close(message_queue);
}

void* display_barcode(void *ptr) {
    char buffer[MAX_SIZE + 1];

    struct mq_attr attr;

    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_SIZE;
    attr.mq_curmsgs = 0;
    attr.mq_flags = 0;

    //mqd_t message_queue = mq_open(MY_QUEUE, O_CREAT | O_RDONLY, 0644, &attr);
    mqd_t message_queue = mq_open(MY_QUEUE, O_CREAT | O_RDONLY, 0644, &attr);

    if (message_queue == (mqd_t) - 1)
    {
        std::cout << "Consumer, Error: " << strerror(errno) << std::endl << "Exiting. " << std::endl;
        exit(-1);
    }

    int exitFlag = 0;
    struct mq_attr attr_work;
    while(exitFlag == 0) {


        sleep(5);
        /*
        In ordner to "non blocking read" we check if there are any messages available and then read from it blocking
        This assumes no other person will access the mq!
        */
        mq_getattr(message_queue, &attr_work);
        std::cout << "currcount" << attr_work.mq_curmsgs << std::endl;
        while(attr_work.mq_curmsgs >0)
        {
            int ret = mq_receive(message_queue, buffer, MAX_SIZE+1, NULL);

            if (ret < 0)
                std::cout << "Error receiving: " << strerror(errno) << std::endl;
            else {
                std::cout << "Received \"" << buffer << "\"" << std::endl; //<< " bytes, buffer: " << buffer << std::endl;
                //if (strcmp(buffer, EXIT_STRING) == 0)
                //  exitFlag = 1;
            }
            mq_getattr(message_queue, &attr_work);
        }
    }

    std::cout << "No more messages from our producer. Too bad..." << std::endl;
    mq_close(message_queue);
    mq_unlink(MY_QUEUE);
}

int main() {
    pthread_t thread1, thread2;

    pthread_create(&thread1, NULL, display_barcode, NULL);
    pthread_create(&thread2, NULL, get_barcode, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

}

