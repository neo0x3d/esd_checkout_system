#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <iostream>
#include <string.h>
#include <fstream>
#include <linux/input.h>
#include <unistd.h>



/*
* Does not fully work, some key presses are not registered, max 1-2 keys
* will be registered
*
*/


/* the defines for the barcode and creditcard reader paths are expectd (hoped)
* to be static and should be moved to the file where the objects are generated (main state machine)
*/
#define BARCODE_READER "/dev/input/by-id/usb-USB_Adapter_USB_Device-event-kbd"
#define CREDITCARD_READER "/dev/input/by-id/usb-c216_0180-event-kbd"


class BCReader {
public:
    BCReader(const char * path);
    ~BCReader();
    //std::string getBarcode(void);
    int getBarcode(std::string* target);
    bool performcheckBarcode = true;
private:
    int checkBarcode(const std::string barcode);
    //int checkBarcode(const char * barcode);
    //std::ifstream charDev;
    int charDevFd;
};


/**
*/
BCReader::BCReader(const char * path)
{
    //charDev.open(path, std::ifstream::in);
    //charDevFd = open(path, O_RDONLY|O_NONBLOCK);
    charDevFd = open(path, O_RDONLY);
    if(charDevFd == -1)
        std::cerr << "barcode_driver: error opening filehandle, returns -1" << std::endl;
}

BCReader::~BCReader()
{
    close(charDevFd);
}

// 
int BCReader::getBarcode(std::string* target)
{
    /*if(charDev.is_open() == false)
    {
        std::cerr << "barcode_driver: trying to read while filehandle is not open!" << std::endl;
        return -1;
    }*/


    std::string buffer = "";
    struct input_event InputEvent;
    char data [sizeof(InputEvent)];
    bool toSend = false;
    int bytesRead = 0;

    while(!toSend)
    {
        //charDev.read(data, sizeof(data));
        bytesRead = read(charDevFd, (void*)data, sizeof(InputEvent));
        std::cout << "read" << bytesRead << std::endl;
        if (bytesRead != sizeof(InputEvent))
        {
            
           if(buffer.length() >= 1)
            {
                std::cerr << "barcode_driver: possible race condition encountered, buffer: " << buffer << " bufferlen: " <<  buffer.length() << std::endl; 
                return -1;
            }
            else
            {
                // buffer empty
                //std::cerr << "barcode_driver: error opening filehandle, returns:" << bytesRead << std::endl;
                return -1;
                
            }
        }
        memcpy(&InputEvent, data, sizeof(InputEvent));
        
        std::cout << "type " << InputEvent.value << " value " << InputEvent.value <<" code " << InputEvent.code<< std::endl;

        // only reacting on keyboard keys and pressed (otherwise it would fire at all state changes)
        if(InputEvent.type == EV_KEY && InputEvent.value == 1)
        {
            toSend = false;
            // key maping for numbers and numpad
            // (enought for the bardode reader, creditcard also requires characters)
            std::cout << "key: " << InputEvent.code << std::endl;
            switch(InputEvent.code)
            {
            case KEY_0:
            case KEY_KP0:
                buffer.append("0");
                break;
            case KEY_1:
            case KEY_KP1:
                buffer.append("1");
                break;
            case KEY_2:
            case KEY_KP2:
                buffer.append("2");
                break;
            case KEY_3:
            case KEY_KP3:
                buffer.append("3");
                break;
            case KEY_4:
            case KEY_KP4:
                buffer.append("4");
                break;
            case KEY_5:
            case KEY_KP5:
                buffer.append("5");
                break;
            case KEY_6:
            case KEY_KP6:
                buffer.append("6");
                break;
            case KEY_7:
            case KEY_KP7:
                buffer.append("7");
                break;
            case KEY_8:
            case KEY_KP8:
                buffer.append("8");
                break;
            case KEY_9:
            case KEY_KP9:
                buffer.append("9");
                break;
            case KEY_ENTER:
            case KEY_KPENTER:
                std::cout << "rec enter" << std::endl;
                toSend = true;
                break;
            default:
                std::cerr << "bercode_driver: unrecognized key: " << InputEvent.code << std::endl;
                break;
            }
        }
        
    }

    
    //TODO checkbarcode here
    
    *target =  buffer;
    //std::string demo = "hello";
    //std::cout << *target.length() << std::endl;
    return (*target).length();
}

/*+@brief checks if barcode could be a valid EAN-13, UPC-A
* @param barcode input to be checked
* @return 0 if conforms, -1 if
*/
int BCReader::checkBarcode(const std::string barcode)
{
    if(barcode.length() != 12 || 13)
        return -1;
    for(int i=0; i<barcode.length(); i++)
    {
        if(!isdigit(barcode[i]))
            return -1;
    }
    return 0;
}

int main(void)
{
    BCReader reader(BARCODE_READER);
    //BCReader reader("/dev/input/event3");
    reader.performcheckBarcode = false;

    std::string current;
    int count = -1;
    
    while(1)
    {
        sleep(5);
        reader.getBarcode(&current);
        std::cout << "out: " << current << ", count: " << count <<std::endl;
        /*do // read all available entries
        {
            count = reader.getBarcode(&current);
            
            if(count>0)
                std::cout << "out: " << current << ", count: " << count <<std::endl;
            else
            {
                if(count == -1)
                    std::cout << "timeout" << std::endl;
                else
                    std::cout << "returned " << count << std::endl;
            }
            
        }while( count >= 0);*/
        
    }


}
