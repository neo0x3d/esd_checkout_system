barcode reader driver


- implement barcode scanner 
- implement input sanitisation
- implement test environment as thread, writing into queue?
- write test 
- write makefile

It is expected that the barcodes are EAN-13 (normal EU barcode found on products in the supermarket)
Does not expect UPC or ISBN codes. 

The driver does not bind to a USB device. Depending on the state of the state machine it should only run in the right time. 

## Files
- demo_barcode_scanner.cpp -> demo of the barcode scanner, using lib
