# esd_checkout_system

Group repository for the Embedded Software Design / Checkout System project

## credit card system
* [ ]  numpad
* [ ]  lcd
* [ ]  card reader
* [ ]  state machine

## Devices / device drivers

* [ ]  lcd 
* [ ]  numpad
* [ ]  zybo/station box
* [x]  customer display (cli is enough, no gui!)
* [ ]  receipt printer
* [ ]  card reader -> Shaza Maher
* [ ]  barcode scanner -> Marius Pfeffer
* [x]  touchscreen (will not use that)
* [ ]  state machine
* [ ]  socket interface (to database) -> Athanasios Papamarinos